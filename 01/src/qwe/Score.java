package qwe;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		double SBefore=0,SBase=0,STest=0,SProgram=0,SAdd=0;
		File sall=new File("small.html");
		Document doc = Jsoup.parse(sall,"UTF-8");
		Elements SAll=doc.getElementsByClass("interaction-row");
		for (int i=0;i<SAll.size();i++)
		{
            //课前自测
			if (SAll.get(i).toString().contains("课前自测"))
			{
				Elements SSpan=SAll.get(i).getElementsByTag("span");
				for (int j=0;j<SSpan.size();j++)
				{
					if (SSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(SSpan.get(j).text());
						SBefore=SBase+sc.nextDouble();
						break;
					}
				}
			}
            //small课堂完成
			else if (SAll.get(i).toString().contains("课堂完成")&&SAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements SSpan=SAll.get(i).getElementsByTag("span");
				for (int j=0;j<SSpan.size();j++)
				{
					if (SSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(SSpan.get(j).text());
						SBase=SBase+sc.nextDouble();
						break;
					}
				}
			}
            //small课堂小测
			else if (SAll.get(i).toString().contains("课堂小测")&&SAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements SSpan=SAll.get(i).getElementsByTag("span");
				for (int j=0;j<SSpan.size();j++)
				{
					if (SSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(SSpan.get(j).text());
						STest=STest+sc.nextDouble();
						break;
					}
				}
			}
            //small编程题
			else if (SAll.get(i).toString().contains("编程题")&&SAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements SSpan=SAll.get(i).getElementsByTag("span");
				for (int j=0;j<SSpan.size();j++)
				{
					if (SSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(SSpan.get(j).text());
						SProgram=SProgram+sc.nextDouble();
						break;
					}
				}
			}
            //small附加题
			else if (SAll.get(i).toString().contains("附加题")&&SAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements SSpan=SAll.get(i).getElementsByTag("span");
				for (int j=0;j<SSpan.size();j++)
				{
					if (SSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(SSpan.get(j).text());
						SAdd=SAdd+sc.nextDouble();
						break;
					}
				}
			}
		}
        //All
		File all=new File("all.html");
		Document Alldoc = Jsoup.parse(all,"UTF-8");
		Elements AllAll=Alldoc.getElementsByClass("interaction-row");
		for (int i=0;i<AllAll.size();i++)
		{
            //All课前自测
			if (AllAll.get(i).toString().contains("课前自测"))
			{
				Elements AllSpan=AllAll.get(i).getElementsByTag("span");
				for (int j=0;j<AllSpan.size();j++)
				{
					if (AllSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(AllSpan.get(j).text());
						SBefore=SBefore+sc.nextDouble();
						break;
					}
				}
			}
		}
        //获取配置文件
		Properties pr=new Properties();
		pr.load(new FileInputStream("src/total.properties"));
		double TotalBefore=Double.parseDouble(pr.getProperty("before"));
		double TotalBase=Double.parseDouble(pr.getProperty("base"));
		double TotalTest=Double.parseDouble(pr.getProperty("test"));
		double TotalProgram=Double.parseDouble(pr.getProperty("program"));
		double TotalAdd=Double.parseDouble(pr.getProperty("add"));
		TotalProgram=95;
		TotalAdd=90;
        //总成绩
		double FinalScore=(SBase/TotalBase)*100*0.3*0.95+(STest/TotalTest)*100*0.2+(SBefore/TotalBefore)*100*0.25+(SProgram/TotalProgram)*100*0.1+(SAdd/TotalAdd)*100*0.05+6;
        //输出总成绩
		System.out.println(FinalScore);
	}

}